## Prerequisites
Install docker-compose (https://docs.docker.com/compose/install/)
## Setup
Edit docker-compose.yml to fetch the correct cve databases for the intended environment, specificaly containers gost and goval(https://github.com/vulsio/gost/blob/master/README.md#fetch-redhat https://github.com/vulsio/goval-dictionary/blob/master/README.md#usage-fetch-oval-data). Additionaly if dessired add jvn database to cve(https://github.com/vulsio/go-cve-dictionary/blob/master/README.md#usage-fetch-command).
In the docker-compose.yml file under the vuls container change the path for the .ssh directory mount point to an appropriate one for the use case, this is where known host keys and the private key for ssh authentication with all target scan machines will be stored(typicaly /home/user-name/.ssh)
Execute the command mkdir vuls-log to initialize the log directory for all containers.
## Install/update cve databases
Run "(sudo) docker-compose up -d" to install or update all databases. Wait until all machines report an "exit 0" state when a "(sudo) docker-compose ps" is executed before proceding with a report.
## Config.toml setup
Edit the slack section of the config.toml file to use the correct hookURL and channel, add notifyUsers if desired. Edit default section as needed for the environment, such as port, user, scan mode. Add all desired servers and ensure that at a minimum parameters host, port, user, sshConfigPath, and keyPath are all set either in the individual server or default config sections.
## Getting ready for scanning via ssh
Note: All hosts including the local machine hosting the docker containers will need to be scanned via ssh, this is because docker containers are their own seperate environment much like a virtual machine.
On this machine (the one that will be hosting the docker containers and vuls) create a private and public ssh key pair by running "ssh-keygen -t rsa (-f /location/of/.ssh/id-rsa)", when asked for a pasphrase leave blank and press Enter. Copy the created id_rsa.pub file (~/.ssh/id_rsa.pub) to the authorized_keys file (/scanningUser/home/.ssh/authorized_keys) on all hosts to be scanned, this may require creating the .ssh directory for the user that will be perform the scan. Back on the scanning system initiate a ssh connection to each machine to be scanned with "ssh username@0.0.0.0 -i ~/.ssh/id_rsa", this will add the host keys to the known_hosts file as well as test that the private key authentication on the target is working.
https://vuls.io/docs/en/tutorial-remote-scan.html 
## Testing vuls config file
To test the config.toml settings you can run (sudo) docker-compose run --rm vuls -c "true && vuls configtest".
## Running a scan/generating a report
Execute (sudo) docker-compose up -d to update all databases, scan all machines, and send the report.